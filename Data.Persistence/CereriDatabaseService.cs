﻿using Microsoft.EntityFrameworkCore;

namespace Data.Persistence
{
    public class CereriDatabaseService : DbContext, ICereriDatabaseService
    {
        public CereriDatabaseService(DbContextOptions<CereriDatabaseService> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Data.Core.Domain.Cerere> Cereri { get; set; }
        public DbSet<Data.Core.Domain.CerereAdeverintaStudent> CereriAdeverintaStudent { get; set; }
        public DbSet<Data.Core.Domain.CerereCTP> CereriCtp { get; set; }
        public DbSet<Data.Core.Domain.CerereLegitimatieTren> CereriLegitimatieTren { get; set; }
        public DbSet<Data.Core.Domain.CerereMarire> CereriMarire { get; set; }
        public DbSet<Data.Core.Domain.CerereMutareGrupa> CereriMutareGrupa { get; set; }
        public DbSet<Data.Core.Domain.CerereSchimbareOptionale> CereriSchimbareOptionale { get; set; }
    }
}
