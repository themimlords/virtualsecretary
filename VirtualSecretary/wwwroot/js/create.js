﻿window.onload = setPartial(localStorage.getItem("tip"));

$("#cereriDropDownList").change(function () {
    var tip_cerere = $(this).find(":selected").val()
    console.log(tip_cerere);
    ajaxCall(tip_cerere);
});

function validate(result) {
    $("#cereriPartial").html("");
    $("#cereriPartial").html(result);
}

function ajaxCall(parameter) {
    $.ajax({
        url: "/Cereri/DynamicPartial",
        contentType: "application/json; charset=utf-8",
        data: parameter,
        type: 'POST',
        success: function (result) {
            validate(result);
        },
        error: function (e) { display(e); }
    });
}

function setPartial(result) {
    console.log(result);
    var dropdown = $("#cereriDropDownList");
    dropdown.val(result);
    ajaxCall(result);
}

function display(e) { console.log(e); }