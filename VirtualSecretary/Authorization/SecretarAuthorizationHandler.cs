﻿using System.Threading.Tasks;
using Data.Core.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace VirtualSecretary.Authorization
{
    public class SecretarAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, Cerere>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement,
                Cerere resource)
        {
            if (context.User == null || resource == null)
            {
                return Task.FromResult(0);
            }

            if (requirement.Name != Constants.AcceptaOperationName &&
                requirement.Name != Constants.RespingeOperationName)
            {
                return Task.FromResult(0);
            }

            if (context.User.IsInRole(Constants.SecretariRole))
            {
                context.Succeed(requirement);
            }

            return Task.FromResult(0);
        }
    }
}
