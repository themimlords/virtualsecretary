﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace VirtualSecretary.Authorization
{
    public static class CereriOperatii
    {
        public static OperationAuthorizationRequirement Create =
            new OperationAuthorizationRequirement { Name = Constants.CreateOperationName };
        public static OperationAuthorizationRequirement Read =
            new OperationAuthorizationRequirement { Name = Constants.ReadOperationName };
        public static OperationAuthorizationRequirement Update =
            new OperationAuthorizationRequirement { Name = Constants.UpdateOperationName };
        public static OperationAuthorizationRequirement Delete =
            new OperationAuthorizationRequirement { Name = Constants.DeleteOperationName };
        public static OperationAuthorizationRequirement Accepta =
            new OperationAuthorizationRequirement { Name = Constants.AcceptaOperationName };
        public static OperationAuthorizationRequirement Respinge =
            new OperationAuthorizationRequirement { Name = Constants.RespingeOperationName };
    }

    public static class Constants
    {
        public static readonly string CreateOperationName = "Create";
        public static readonly string ReadOperationName = "Read";
        public static readonly string UpdateOperationName = "Update";
        public static readonly string DeleteOperationName = "Delete";
        public static readonly string AcceptaOperationName = "Acceptat";
        public static readonly string RespingeOperationName = "Respins";

        public static readonly string AdminsRole = "Admins";
        public static readonly string SecretariRole = "Secretari";
    }
}
