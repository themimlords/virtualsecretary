using System.Threading.Tasks;

namespace VirtualSecretary.Services
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link)
        {
            return emailSender.SendEmailAsync(email, "Confirmare email",
                $"Va rugam sa va confirmati contul prin intermediul acestui link: {link}");
        }
    }
}
