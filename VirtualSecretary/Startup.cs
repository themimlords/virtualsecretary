﻿using System;
using System.Threading.Tasks;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VirtualSecretary.Authorization;
using VirtualSecretary.Data;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<CereriDatabaseService>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("CereriConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(config =>
                {
                    config.SignIn.RequireConfirmedEmail = true;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            //services.AddMvc();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });

            services.Configure<AuthMessageSenderOptions>(Configuration);

            services.AddScoped<IAuthorizationHandler, IsOwnerAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, AdminAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, SecretarAuthorizationHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            IServiceScopeFactory scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();

            using (IServiceScope scope = scopeFactory.CreateScope())
            {
                RoleManager<IdentityRole> roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                UserManager<ApplicationUser> userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                // Seed database code goes here
                CreateRoles(roleManager, userManager).Wait();
            }
        }

        private async Task CreateRoles(RoleManager<IdentityRole> RoleManager, UserManager<ApplicationUser> UserManager)
        {
            string[] roleNames = { "Admin", "Secretar"};
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            var admin = new ApplicationUser
            {
                UserName = "admin@virtualsecretary.com",
                Email = "admin@virtualsecretary.com",
            };

            string adminPWD = Configuration["AdminPW"];
            var _adminUser = await UserManager.FindByEmailAsync("admin@virtualsecretary.com");

            if (_adminUser == null)
            {
                var createAdminUser = await UserManager.CreateAsync(admin, adminPWD);
                if (createAdminUser.Succeeded)
                {
                    await UserManager.AddToRoleAsync(admin, "Admin");
                }
            }

            var secretar = new ApplicationUser
            {
                UserName = "virtualsecretaryinfo@gmail.com",
                Email = "virtualsecretaryinfo@gmail.com",
            };

            string secretarPWD = Configuration["SecretarPW"];
            var _secretarUser = await UserManager.FindByEmailAsync("virtualsecretaryinfo@gmail.com");

            if (_secretarUser == null)
            {
                var createSecretarUser = await UserManager.CreateAsync(secretar, secretarPWD);
                if (createSecretarUser.Succeeded)
                {
                    await UserManager.AddToRoleAsync(secretar, "Secretar");
                }
            }
        }

    }
}
