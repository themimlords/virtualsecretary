﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data.Core.Domain;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using VirtualSecretary.Authorization;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary.Controllers
{
    public class CereriMutareGrupaController : Controller
    {
        private readonly CereriDatabaseService _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public CereriMutareGrupaController(CereriDatabaseService context, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _emailSender = emailSender;
        }

        // GET: CereriMutareGrupa
        public async Task<IActionResult> Index()
        {
            var cereriMutareGrupa = from c in _context.CereriMutareGrupa select c;
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            var currentUserId = _userManager.GetUserId(User);
            if (!isAuthorized)
            {
                cereriMutareGrupa = cereriMutareGrupa.Where(c => c.OwnerID == currentUserId);
            }
            return View(await cereriMutareGrupa.ToListAsync());
        }

        // GET: CereriMutareGrupa/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereMutareGrupa = await _context.CereriMutareGrupa
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereMutareGrupa == null)
            {
                return NotFound();
            }

            return View(cerereMutareGrupa);
        }

        // GET: CereriMutareGrupa/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CereriMutareGrupa/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GrupaVeche,GrupaNoua,Id,OwnerID,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereMutareGrupa cerereMutareGrupa)
        {
            if (ModelState.IsValid)
            {
                cerereMutareGrupa.Id = Guid.NewGuid();
                cerereMutareGrupa.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereMutareGrupa, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereMutareGrupa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cerereMutareGrupa);
        }

        // GET: CereriMutareGrupa/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereMutareGrupa = await _context.CereriMutareGrupa.SingleOrDefaultAsync(m => m.Id == id);
            if (cerereMutareGrupa == null)
            {
                return NotFound();
            }
            return View(cerereMutareGrupa);
        }

        // POST: CereriMutareGrupa/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("GrupaVeche,GrupaNoua,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereMutareGrupa cerereMutareGrupa)
        {
            cerereMutareGrupa.OwnerID = _userManager.GetUserId(User);
            if (id != cerereMutareGrupa.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cerereMutareGrupa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CerereMutareGrupaExists(cerereMutareGrupa.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cerereMutareGrupa);
        }

        // GET: CereriMutareGrupa/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereMutareGrupa = await _context.CereriMutareGrupa
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereMutareGrupa == null)
            {
                return NotFound();
            }

            return View(cerereMutareGrupa);
        }

        // POST: CereriMutareGrupa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var cerereMutareGrupa = await _context.CereriMutareGrupa.SingleOrDefaultAsync(m => m.Id == id);
            _context.CereriMutareGrupa.Remove(cerereMutareGrupa);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CerereMutareGrupaExists(Guid id)
        {
            return _context.CereriMutareGrupa.Any(e => e.Id == id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetStatus(Guid id, Cerere.StareCerere status,String motiv)
        {
            var cerereMutareGrupa = await _context.CereriMutareGrupa.SingleOrDefaultAsync(m => m.Id == id);
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");

            if (!isAuthorized)
            {
                return new ChallengeResult();
            }
            cerereMutareGrupa.Stare = status;
            _context.CereriMutareGrupa.Update(cerereMutareGrupa);
            await _context.SaveChangesAsync();

            if (status == Cerere.StareCerere.Aprobata)
            {
                await _emailSender.SendEmailAsync(cerereMutareGrupa.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereMutareGrupa.Id + " a fost " + status + ". \n\nDetaliile cererii:" +
                    "\n\tGrupa noua: " + cerereMutareGrupa.GrupaNoua +
                    "\n\tGrupa veche: " + cerereMutareGrupa.GrupaVeche +
                    "\n\tNume: " + cerereMutareGrupa.Nume +
                    "\n\tPrenume: " + cerereMutareGrupa.Prenume +
                    "\n\tInitiala tatalui: " + cerereMutareGrupa.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereMutareGrupa.NumarBuletin +
                    "\n\tNumar matricol: " + cerereMutareGrupa.NumarMatricol +
                    "\n\tSerie buletin: " + cerereMutareGrupa.SerieBuletin);
            }
            else
            {
                await _emailSender.SendEmailAsync(cerereMutareGrupa.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereMutareGrupa.Id + " a fost " + status + ".\nMotivul respingerii: "+motiv+" \n\nDetaliile cererii:" +
                    "\n\tGrupa noua: " + cerereMutareGrupa.GrupaNoua +
                    "\n\tGrupa veche: " + cerereMutareGrupa.GrupaVeche +
                    "\n\tNume: " + cerereMutareGrupa.Nume +
                    "\n\tPrenume: " + cerereMutareGrupa.Prenume +
                    "\n\tInitiala tatalui: " + cerereMutareGrupa.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereMutareGrupa.NumarBuletin +
                    "\n\tNumar matricol: " + cerereMutareGrupa.NumarMatricol +
                    "\n\tSerie buletin: " + cerereMutareGrupa.SerieBuletin);
            }

            return RedirectToAction("Index");
        }
    }
}
