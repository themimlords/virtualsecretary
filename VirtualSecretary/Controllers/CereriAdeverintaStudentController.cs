﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data.Core.Domain;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using VirtualSecretary.Authorization;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary.Controllers
{
    public class CereriAdeverintaStudentController : Controller
    {
        private readonly CereriDatabaseService _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public CereriAdeverintaStudentController(CereriDatabaseService context, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager,IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _emailSender = emailSender;
        }

        // GET: CereriAdeverintaStudent
        public async Task<IActionResult> Index()
        {
            var cereriAdeverintaStudent = from c in _context.CereriAdeverintaStudent select c;
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            var currentUserId = _userManager.GetUserId(User);
            if (!isAuthorized)
            {
                cereriAdeverintaStudent = cereriAdeverintaStudent.Where(c => c.OwnerID == currentUserId);
            }
            return View(await cereriAdeverintaStudent.ToListAsync());
        }

        // GET: CereriAdeverintaStudent/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereAdeverintaStudent = await _context.CereriAdeverintaStudent
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereAdeverintaStudent == null)
            {
                return NotFound();
            }

            return View(cerereAdeverintaStudent);
        }

        // GET: CereriAdeverintaStudent/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CereriAdeverintaStudent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AnUniversitar,MotivEliberare,Id,OwnerID,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereAdeverintaStudent cerereAdeverintaStudent)
        {
            if (ModelState.IsValid)
            {
                cerereAdeverintaStudent.Id = Guid.NewGuid();
                cerereAdeverintaStudent.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereAdeverintaStudent, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereAdeverintaStudent);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cerereAdeverintaStudent);
        }

        // GET: CereriAdeverintaStudent/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereAdeverintaStudent = await _context.CereriAdeverintaStudent.SingleOrDefaultAsync(m => m.Id == id);
            if (cerereAdeverintaStudent == null)
            {
                return NotFound();
            }
            return View(cerereAdeverintaStudent);
        }

        // POST: CereriAdeverintaStudent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("AnUniversitar,MotivEliberare,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereAdeverintaStudent cerereAdeverintaStudent)
        {
            cerereAdeverintaStudent.OwnerID = _userManager.GetUserId(User);
            if (id != cerereAdeverintaStudent.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cerereAdeverintaStudent);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CerereAdeverintaStudentExists(cerereAdeverintaStudent.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cerereAdeverintaStudent);
        }

        // GET: CereriAdeverintaStudent/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereAdeverintaStudent = await _context.CereriAdeverintaStudent
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereAdeverintaStudent == null)
            {
                return NotFound();
            }

            return View(cerereAdeverintaStudent);
        }

        // POST: CereriAdeverintaStudent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var cerereAdeverintaStudent = await _context.CereriAdeverintaStudent.SingleOrDefaultAsync(m => m.Id == id);
            _context.CereriAdeverintaStudent.Remove(cerereAdeverintaStudent);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CerereAdeverintaStudentExists(Guid id)
        {
            return _context.CereriAdeverintaStudent.Any(e => e.Id == id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetStatus(Guid id, Cerere.StareCerere status, String motiv)
        {
            var cerereAdeverintaStudent = await _context.CereriAdeverintaStudent.SingleOrDefaultAsync(m => m.Id == id);
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            
            if (!isAuthorized)
            {
                return new ChallengeResult();
            }
            cerereAdeverintaStudent.Stare = status;
            _context.CereriAdeverintaStudent.Update(cerereAdeverintaStudent);
            await _context.SaveChangesAsync();
            if (status == Cerere.StareCerere.Aprobata)
            {
                await _emailSender.SendEmailAsync(cerereAdeverintaStudent.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereAdeverintaStudent.Id + " a fost " + status + ". \n\nDetaliile cererii:" +
                    "\n\tAn universitar: " + cerereAdeverintaStudent.AnUniversitar +
                    "\n\tMotiv eliberare: " + cerereAdeverintaStudent.MotivEliberare +
                    "\n\tNume: " + cerereAdeverintaStudent.Nume +
                    "\n\tPrenume: " + cerereAdeverintaStudent.Prenume +
                    "\n\tInitiala tatalui: " + cerereAdeverintaStudent.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereAdeverintaStudent.NumarBuletin +
                    "\n\tNumar matricol: " + cerereAdeverintaStudent.NumarMatricol +
                    "\n\tSerie buletin: " + cerereAdeverintaStudent.SerieBuletin);
            }
            else
            {
                await _emailSender.SendEmailAsync(cerereAdeverintaStudent.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereAdeverintaStudent.Id + " a fost " + status + 
                    ".\nMotivul respingerii: "+ motiv +
                    "\n\nDetaliile cererii:" +
                    "\n\tAn universitar: " + cerereAdeverintaStudent.AnUniversitar +
                    "\n\tMotiv eliberare: " + cerereAdeverintaStudent.MotivEliberare +
                    "\n\tNume: " + cerereAdeverintaStudent.Nume +
                    "\n\tPrenume: " + cerereAdeverintaStudent.Prenume +
                    "\n\tInitiala tatalui: " + cerereAdeverintaStudent.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereAdeverintaStudent.NumarBuletin +
                    "\n\tNumar matricol: " + cerereAdeverintaStudent.NumarMatricol +
                    "\n\tSerie buletin: " + cerereAdeverintaStudent.SerieBuletin);
            }
            return RedirectToAction("Index");
        }
    }
}
