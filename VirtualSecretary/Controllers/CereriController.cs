﻿using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data.Core.Domain;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using VirtualSecretary.Authorization;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary.Controllers
{
    public class CereriController : Controller
    {
        private readonly CereriDatabaseService _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;
        public AuthMessageSenderOptions Options { get; }


        public CereriController(CereriDatabaseService context, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _emailSender = emailSender;
        }

        // GET: Cereri
        public async Task<IActionResult> Index()
        {
            var cereri = from c in _context.Cereri select c;
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            var currentUserId = _userManager.GetUserId(User);
            if (!isAuthorized)
            {
                cereri = cereri.Where(c => c.OwnerID == currentUserId);
            }

            return View(await cereri.ToListAsync());
        }

        // GET: Cereri/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCerereAdeverintaStudent([Bind("AnUniversitar,MotivEliberare,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,StareCerere")] CerereAdeverintaStudent cerereAdeverintaStudent)
        {
            if (ModelState.IsValid)
            {
                cerereAdeverintaStudent.Id = Guid.NewGuid();
                cerereAdeverintaStudent.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereAdeverintaStudent, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereAdeverintaStudent);
                await _context.SaveChangesAsync();

                var secretariat = "virtualsecretaryinfo@gmail.com";
                await _emailSender.SendEmailAsync(secretariat, "Cerere noua",
                    "Studentul " + cerereAdeverintaStudent.Nume + " " + cerereAdeverintaStudent.Prenume +
                    " a depus o cerere de tip Adeverință student." + "\n\nDetaliile cererii:" +
                    "\n\tAn universitar: " + cerereAdeverintaStudent.AnUniversitar +
                    "\n\tMotiv eliberare: " + cerereAdeverintaStudent.MotivEliberare +
                    "\n\tNume: " + cerereAdeverintaStudent.Nume +
                    "\n\tPrenume: " + cerereAdeverintaStudent.Prenume +
                    "\n\tInitiala tatalui: " + cerereAdeverintaStudent.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereAdeverintaStudent.NumarBuletin +
                    "\n\tNumar matricol: " + cerereAdeverintaStudent.NumarMatricol +
                    "\n\tSerie buletin: " + cerereAdeverintaStudent.SerieBuletin);

                return RedirectToAction(nameof(Index));
            }
            return PartialView("\\Views\\CereriAdeverintaStudent\\Create.cshtml", cerereAdeverintaStudent);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCerereCtp([Bind("Data,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,StareCerere")] CerereCTP cerereCtp)
        {
            if (ModelState.IsValid)
            {
                cerereCtp.Id = Guid.NewGuid();
                cerereCtp.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereCtp, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereCtp);
                await _context.SaveChangesAsync();

                var secretariat = "virtualsecretaryinfo@gmail.com";
                await _emailSender.SendEmailAsync(secretariat, "Cerere noua",
                    "Studentul " + cerereCtp.Nume + " " + cerereCtp.Prenume +
                    " a depus o cerere de tip CTP." + "\n\nDetaliile cererii:" +
                    "\n\tData: " + cerereCtp.Data +
                    "\n\tNume: " + cerereCtp.Nume +
                    "\n\tPrenume: " + cerereCtp.Prenume +
                    "\n\tInitiala tatalui: " + cerereCtp.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereCtp.NumarBuletin +
                    "\n\tNumar matricol: " + cerereCtp.NumarMatricol +
                    "\n\tSerie buletin: " + cerereCtp.SerieBuletin);

                return RedirectToAction(nameof(Index));
            }
            return PartialView("\\Views\\CereriCtp\\Create.cshtml", cerereCtp);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCerereLegitimatieTren([Bind("Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,StareCerere")] CerereLegitimatieTren cerereLegitimatieTren)
        {
            if (ModelState.IsValid)
            {
                cerereLegitimatieTren.Id = Guid.NewGuid();
                cerereLegitimatieTren.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereLegitimatieTren, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereLegitimatieTren);
                await _context.SaveChangesAsync();

                var secretariat = "virtualsecretaryinfo@gmail.com";
                await _emailSender.SendEmailAsync(secretariat, "Cerere noua",
                    "Studentul " + cerereLegitimatieTren.Nume + " " + cerereLegitimatieTren.Prenume +
                    " a depus o cerere de tip Legitimație tren." + "\n\nDetaliile cererii:" +
                    "\n\tNume: " + cerereLegitimatieTren.Nume +
                    "\n\tPrenume: " + cerereLegitimatieTren.Prenume +
                    "\n\tInitiala tatalui: " + cerereLegitimatieTren.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereLegitimatieTren.NumarBuletin +
                    "\n\tNumar matricol: " + cerereLegitimatieTren.NumarMatricol +
                    "\n\tSerie buletin: " + cerereLegitimatieTren.SerieBuletin);


                return RedirectToAction(nameof(Index));
            }
            return PartialView("\\Views\\CereriLegitimatieTren\\Create.cshtml", cerereLegitimatieTren);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCerereMutareGrupa([Bind("GrupaVeche,GrupaNoua,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereMutareGrupa cerereMutareGrupa)
        {
            if (ModelState.IsValid)
            {
                cerereMutareGrupa.Id = Guid.NewGuid();
                cerereMutareGrupa.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereMutareGrupa, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereMutareGrupa);
                await _context.SaveChangesAsync();

                var secretariat = "virtualsecretaryinfo@gmail.com";
                await _emailSender.SendEmailAsync(secretariat, "Cerere noua",
                    "Studentul " + cerereMutareGrupa.Nume + " " + cerereMutareGrupa.Prenume +
                    " a depus o cerere de tip Mutare grupă." + "\n\nDetaliile cererii:" +
                    "\n\tGrupa noua: " + cerereMutareGrupa.GrupaNoua +
                    "\n\tGrupa veche: " + cerereMutareGrupa.GrupaVeche +
                    "\n\tNume: " + cerereMutareGrupa.Nume +
                    "\n\tPrenume: " + cerereMutareGrupa.Prenume +
                    "\n\tInitiala tatalui: " + cerereMutareGrupa.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereMutareGrupa.NumarBuletin +
                    "\n\tNumar matricol: " + cerereMutareGrupa.NumarMatricol +
                    "\n\tSerie buletin: " + cerereMutareGrupa.SerieBuletin);

                return RedirectToAction(nameof(Index));
            }
            return PartialView("\\Views\\CereriMutareGrupa\\Create.cshtml", cerereMutareGrupa);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCerereMarire([Bind("Materie,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,StareCerere")] CerereMarire cerereMarire)
        {
            if (ModelState.IsValid)
            {
                cerereMarire.Id = Guid.NewGuid();
                cerereMarire.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereMarire, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereMarire);
                await _context.SaveChangesAsync();

                var secretariat = "virtualsecretaryinfo@gmail.com";
                await _emailSender.SendEmailAsync(secretariat, "Cerere noua",
                    "Studentul " + cerereMarire.Nume + " " + cerereMarire.Prenume +
                    " a depus o cerere de tip Mărire notă." + "\n\nDetaliile cererii:" +
                    "\n\tMaterie: " + cerereMarire.Materie +
                    "\n\tNume: " + cerereMarire.Nume +
                    "\n\tPrenume: " + cerereMarire.Prenume +
                    "\n\tInitiala tatalui: " + cerereMarire.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereMarire.NumarBuletin +
                    "\n\tNumar matricol: " + cerereMarire.NumarMatricol +
                    "\n\tSerie buletin: " + cerereMarire.SerieBuletin);

                return RedirectToAction(nameof(Index));
            }
            return PartialView("\\Views\\CereriMarire\\Create.cshtml", cerereMarire);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCerereSchimbareOptionale([Bind("OptionalVechi,OptionalNou,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,StareCerere")] CerereSchimbareOptionale cerereSchimbareOptionale)
        {
            if (ModelState.IsValid)
            {
                cerereSchimbareOptionale.Id = Guid.NewGuid();
                cerereSchimbareOptionale.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereSchimbareOptionale, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereSchimbareOptionale);
                await _context.SaveChangesAsync();

                var secretariat = "virtualsecretaryinfo@gmail.com";
                await _emailSender.SendEmailAsync(secretariat, "Cerere noua",
                    "Studentul " + cerereSchimbareOptionale.Nume + " " + cerereSchimbareOptionale.Prenume +
                    " a depus o cerere de tip Schimbare opțional." + "\n\nDetaliile cererii:" +
                    "\n\tOptional nou: " + cerereSchimbareOptionale.OptionalNou +
                    "\n\tOptional vechi: " + cerereSchimbareOptionale.OptionalVechi +
                    "\n\tNume: " + cerereSchimbareOptionale.Nume +
                    "\n\tPrenume: " + cerereSchimbareOptionale.Prenume +
                    "\n\tInitiala tatalui: " + cerereSchimbareOptionale.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereSchimbareOptionale.NumarBuletin +
                    "\n\tNumar matricol: " + cerereSchimbareOptionale.NumarMatricol +
                    "\n\tSerie buletin: " + cerereSchimbareOptionale.SerieBuletin);

                return RedirectToAction(nameof(Index));
            }
            return PartialView("\\Views\\CereriSchimbareOptionale\\Create.cshtml", cerereSchimbareOptionale);
        }

        [HttpPost]
        public ActionResult DynamicPartial()
        {
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                string cerere = reader.ReadToEnd();
                switch (cerere)
                {
                    case "AdeverintaStudent": return PartialView("_CerereAdeverintaStudent");
                    case "CTP": return PartialView("_CerereCtp");
                    case "LegitimatieTren": return PartialView("_CerereLegitimatieTren");
                    case "Marire": return PartialView("_CerereMarire");
                    case "MutareGrupa": return PartialView("_CerereMutareGrupa");
                    case "SchimbareOptionale": return PartialView("_CerereSchimbareOptionale");
                    default: return PartialView("_Default");
                }
            }
        }
    }
}
