﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data.Core.Domain;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using VirtualSecretary.Authorization;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary.Controllers
{
    public class CereriSchimbareOptionaleController : Controller
    {
        private readonly CereriDatabaseService _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public CereriSchimbareOptionaleController(CereriDatabaseService context, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _emailSender = emailSender;
        }

        // GET: CereriSchimbareOptionale
        public async Task<IActionResult> Index()
        {
            var cereriSchimbareOptionale = from c in _context.CereriSchimbareOptionale select c;
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            var currentUserId = _userManager.GetUserId(User);
            if (!isAuthorized)
            {
                cereriSchimbareOptionale = cereriSchimbareOptionale.Where(c => c.OwnerID == currentUserId);
            }
            return View(await cereriSchimbareOptionale.ToListAsync());
        }

        // GET: CereriSchimbareOptionale/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereSchimbareOptionale = await _context.CereriSchimbareOptionale
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereSchimbareOptionale == null)
            {
                return NotFound();
            }

            return View(cerereSchimbareOptionale);
        }

        // GET: CereriSchimbareOptionale/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CereriSchimbareOptionale/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OptionalVechi,OptionalNou,Id,OwnerID,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereSchimbareOptionale cerereSchimbareOptionale)
        {
            if (ModelState.IsValid)
            {
                cerereSchimbareOptionale.Id = Guid.NewGuid();
                cerereSchimbareOptionale.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereSchimbareOptionale, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereSchimbareOptionale);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cerereSchimbareOptionale);
        }

        // GET: CereriSchimbareOptionale/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereSchimbareOptionale = await _context.CereriSchimbareOptionale.SingleOrDefaultAsync(m => m.Id == id);
            if (cerereSchimbareOptionale == null)
            {
                return NotFound();
            }
            return View(cerereSchimbareOptionale);
        }

        // POST: CereriSchimbareOptionale/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("OptionalVechi,OptionalNou,Id,OwnerID,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereSchimbareOptionale cerereSchimbareOptionale)
        {
            cerereSchimbareOptionale.OwnerID = _userManager.GetUserId(User);
            if (id != cerereSchimbareOptionale.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cerereSchimbareOptionale);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CerereSchimbareOptionaleExists(cerereSchimbareOptionale.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cerereSchimbareOptionale);
        }

        // GET: CereriSchimbareOptionale/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereSchimbareOptionale = await _context.CereriSchimbareOptionale
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereSchimbareOptionale == null)
            {
                return NotFound();
            }

            return View(cerereSchimbareOptionale);
        }

        // POST: CereriSchimbareOptionale/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var cerereSchimbareOptionale = await _context.CereriSchimbareOptionale.SingleOrDefaultAsync(m => m.Id == id);
            _context.CereriSchimbareOptionale.Remove(cerereSchimbareOptionale);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CerereSchimbareOptionaleExists(Guid id)
        {
            return _context.CereriSchimbareOptionale.Any(e => e.Id == id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetStatus(Guid id, Cerere.StareCerere status,String motiv)
        {
            var cerereSchimbareOptionale = await _context.CereriSchimbareOptionale.SingleOrDefaultAsync(m => m.Id == id);
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");

            if (!isAuthorized)
            {
                return new ChallengeResult();
            }
            cerereSchimbareOptionale.Stare = status;
            _context.CereriSchimbareOptionale.Update(cerereSchimbareOptionale);
            await _context.SaveChangesAsync();

            if (status == Cerere.StareCerere.Aprobata)
            {
                await _emailSender.SendEmailAsync(cerereSchimbareOptionale.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereSchimbareOptionale.Id + " a fost " + status + ". \n\nDetaliile cererii:" +
                    "\n\tOptional nou: " + cerereSchimbareOptionale.OptionalNou +
                    "\n\tOptional vechi: " + cerereSchimbareOptionale.OptionalVechi +
                    "\n\tNume: " + cerereSchimbareOptionale.Nume +
                    "\n\tPrenume: " + cerereSchimbareOptionale.Prenume +
                    "\n\tInitiala tatalui: " + cerereSchimbareOptionale.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereSchimbareOptionale.NumarBuletin +
                    "\n\tNumar matricol: " + cerereSchimbareOptionale.NumarMatricol +
                    "\n\tSerie buletin: " + cerereSchimbareOptionale.SerieBuletin);
            }
            else
            {
                await _emailSender.SendEmailAsync(cerereSchimbareOptionale.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereSchimbareOptionale.Id + " a fost " + status + ".\nMotivul respingerii: "+motiv+" \n\nDetaliile cererii:" +
                    "\n\tOptional nou: " + cerereSchimbareOptionale.OptionalNou +
                    "\n\tOptional vechi: " + cerereSchimbareOptionale.OptionalVechi +
                    "\n\tNume: " + cerereSchimbareOptionale.Nume +
                    "\n\tPrenume: " + cerereSchimbareOptionale.Prenume +
                    "\n\tInitiala tatalui: " + cerereSchimbareOptionale.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereSchimbareOptionale.NumarBuletin +
                    "\n\tNumar matricol: " + cerereSchimbareOptionale.NumarMatricol +
                    "\n\tSerie buletin: " + cerereSchimbareOptionale.SerieBuletin);
            }
            return RedirectToAction("Index");
        }
    }
}
