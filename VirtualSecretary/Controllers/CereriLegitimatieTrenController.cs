﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data.Core.Domain;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using VirtualSecretary.Authorization;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary.Controllers
{
    public class CereriLegitimatieTrenController : Controller
    {
        private readonly CereriDatabaseService _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public CereriLegitimatieTrenController(CereriDatabaseService context, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _emailSender = emailSender;
        }

        // GET: CereriLegitimatieTren
        public async Task<IActionResult> Index()
        {
            var cereriLegitimatieTren = from c in _context.CereriLegitimatieTren select c;
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            var currentUserId = _userManager.GetUserId(User);
            if (!isAuthorized)
            {
                cereriLegitimatieTren = cereriLegitimatieTren.Where(c => c.OwnerID == currentUserId);
            }
            return View(await cereriLegitimatieTren.ToListAsync());
        }

        // GET: CereriLegitimatieTren/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereLegitimatieTren = await _context.CereriLegitimatieTren
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereLegitimatieTren == null)
            {
                return NotFound();
            }

            return View(cerereLegitimatieTren);
        }

        // GET: CereriLegitimatieTren/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CereriLegitimatieTren/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,OwnerID,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereLegitimatieTren cerereLegitimatieTren)
        {
            if (ModelState.IsValid)
            {
                cerereLegitimatieTren.Id = Guid.NewGuid();
                cerereLegitimatieTren.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereLegitimatieTren, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereLegitimatieTren);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cerereLegitimatieTren);
        }

        // GET: CereriLegitimatieTren/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereLegitimatieTren = await _context.CereriLegitimatieTren.SingleOrDefaultAsync(m => m.Id == id);
            if (cerereLegitimatieTren == null)
            {
                return NotFound();
            }
            return View(cerereLegitimatieTren);
        }

        // POST: CereriLegitimatieTren/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereLegitimatieTren cerereLegitimatieTren)
        {
            cerereLegitimatieTren.OwnerID = _userManager.GetUserId(User);
            if (id != cerereLegitimatieTren.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cerereLegitimatieTren);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CerereLegitimatieTrenExists(cerereLegitimatieTren.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cerereLegitimatieTren);
        }

        // GET: CereriLegitimatieTren/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereLegitimatieTren = await _context.CereriLegitimatieTren
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereLegitimatieTren == null)
            {
                return NotFound();
            }

            return View(cerereLegitimatieTren);
        }

        // POST: CereriLegitimatieTren/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var cerereLegitimatieTren = await _context.CereriLegitimatieTren.SingleOrDefaultAsync(m => m.Id == id);
            _context.CereriLegitimatieTren.Remove(cerereLegitimatieTren);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CerereLegitimatieTrenExists(Guid id)
        {
            return _context.CereriLegitimatieTren.Any(e => e.Id == id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetStatus(Guid id, Cerere.StareCerere status,String motiv)
        {
            var cerereLegitimatieTren = await _context.CereriLegitimatieTren.SingleOrDefaultAsync(m => m.Id == id);
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");

            if (!isAuthorized)
            {
                return new ChallengeResult();
            }
            cerereLegitimatieTren.Stare = status;
            _context.CereriLegitimatieTren.Update(cerereLegitimatieTren);
            await _context.SaveChangesAsync();

            if (status == Cerere.StareCerere.Aprobata)
            {
                await _emailSender.SendEmailAsync(cerereLegitimatieTren.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereLegitimatieTren.Id + " a fost " + status + ". \n\nDetaliile cererii:" +
                    "\n\tNume: " + cerereLegitimatieTren.Nume +
                    "\n\tPrenume: " + cerereLegitimatieTren.Prenume +
                    "\n\tInitiala tatalui: " + cerereLegitimatieTren.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereLegitimatieTren.NumarBuletin +
                    "\n\tNumar matricol: " + cerereLegitimatieTren.NumarMatricol +
                    "\n\tSerie buletin: " + cerereLegitimatieTren.SerieBuletin);
            }
            else
            {
                await _emailSender.SendEmailAsync(cerereLegitimatieTren.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereLegitimatieTren.Id + " a fost " + status + ".\nMotivul respingerii: "+motiv+" \n\nDetaliile cererii:" +
                    "\n\tNume: " + cerereLegitimatieTren.Nume +
                    "\n\tPrenume: " + cerereLegitimatieTren.Prenume +
                    "\n\tInitiala tatalui: " + cerereLegitimatieTren.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereLegitimatieTren.NumarBuletin +
                    "\n\tNumar matricol: " + cerereLegitimatieTren.NumarMatricol +
                    "\n\tSerie buletin: " + cerereLegitimatieTren.SerieBuletin);
            }
            return RedirectToAction("Index");
        }
    }
}
