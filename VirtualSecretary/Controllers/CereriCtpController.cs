﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data.Core.Domain;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using VirtualSecretary.Authorization;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary.Controllers
{
    public class CereriCtpController : Controller
    {
        private readonly CereriDatabaseService _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public CereriCtpController(CereriDatabaseService context, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _emailSender = emailSender;
        }

        // GET: CereriCtp
        public async Task<IActionResult> Index()
        {
            var cereriCtp = from c in _context.CereriCtp select c;
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            var currentUserId = _userManager.GetUserId(User);
            if (!isAuthorized)
            {
                cereriCtp = cereriCtp.Where(c => c.OwnerID == currentUserId);
            }
            return View(await cereriCtp.ToListAsync());
        }

        // GET: CereriCtp/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereCTP = await _context.CereriCtp
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereCTP == null)
            {
                return NotFound();
            }

            return View(cerereCTP);
        }

        // GET: CereriCtp/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CereriCtp/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Data,Id,OwnerID,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereCTP cerereCTP)
        {
            if (ModelState.IsValid)
            {
                cerereCTP.Id = Guid.NewGuid();
                cerereCTP.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereCTP, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereCTP);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cerereCTP);
        }

        // GET: CereriCtp/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereCTP = await _context.CereriCtp.SingleOrDefaultAsync(m => m.Id == id);
            if (cerereCTP == null)
            {
                return NotFound();
            }
            return View(cerereCTP);
        }

        // POST: CereriCtp/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Data,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereCTP cerereCTP)
        {
            cerereCTP.OwnerID = _userManager.GetUserId(User);
            if (id != cerereCTP.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cerereCTP);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CerereCTPExists(cerereCTP.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cerereCTP);
        }

        // GET: CereriCtp/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereCTP = await _context.CereriCtp
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereCTP == null)
            {
                return NotFound();
            }

            return View(cerereCTP);
        }

        // POST: CereriCtp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var cerereCTP = await _context.CereriCtp.SingleOrDefaultAsync(m => m.Id == id);
            _context.CereriCtp.Remove(cerereCTP);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CerereCTPExists(Guid id)
        {
            return _context.CereriCtp.Any(e => e.Id == id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetStatus(Guid id, Cerere.StareCerere status,String motiv)
        {
            var cerereCtp = await _context.CereriCtp.SingleOrDefaultAsync(m => m.Id == id);
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");

            if (!isAuthorized)
            {
                return new ChallengeResult();
            }
            cerereCtp.Stare = status;
            _context.CereriCtp.Update(cerereCtp);
            await _context.SaveChangesAsync();

            if (status == Cerere.StareCerere.Aprobata)
            {
                await _emailSender.SendEmailAsync(cerereCtp.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereCtp.Id + " a fost " + status + ". \n\nDetaliile cererii:" +
                    "\n\tData: " + cerereCtp.Data +
                    "\n\tNume: " + cerereCtp.Nume +
                    "\n\tPrenume: " + cerereCtp.Prenume +
                    "\n\tInitiala tatalui: " + cerereCtp.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereCtp.NumarBuletin +
                    "\n\tNumar matricol: " + cerereCtp.NumarMatricol +
                    "\n\tSerie buletin: " + cerereCtp.SerieBuletin);
            }
            else
            {
                await _emailSender.SendEmailAsync(cerereCtp.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereCtp.Id + " a fost " + status + ".\nMotivul respingerii: "+motiv+" \n\nDetaliile cererii:" +
                    "\n\tData: " + cerereCtp.Data +
                    "\n\tNume: " + cerereCtp.Nume +
                    "\n\tPrenume: " + cerereCtp.Prenume +
                    "\n\tInitiala tatalui: " + cerereCtp.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereCtp.NumarBuletin +
                    "\n\tNumar matricol: " + cerereCtp.NumarMatricol +
                    "\n\tSerie buletin: " + cerereCtp.SerieBuletin);
            }
            return RedirectToAction("Index");
        }
    }
}
