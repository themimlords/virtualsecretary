﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data.Core.Domain;
using Data.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using VirtualSecretary.Authorization;
using VirtualSecretary.Models;
using VirtualSecretary.Services;

namespace VirtualSecretary.Controllers
{
    public class CereriMarireController : Controller
    {
        private readonly CereriDatabaseService _context;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;

        public CereriMarireController(CereriDatabaseService context, IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _emailSender = emailSender;
        }

        // GET: CereriMarire
        public async Task<IActionResult> Index()
        {
            var cereriMarire = from c in _context.CereriMarire select c;
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");
            var currentUserId = _userManager.GetUserId(User);
            if (!isAuthorized)
            {
                cereriMarire = cereriMarire.Where(c => c.OwnerID == currentUserId);
            }
            return View(await cereriMarire.ToListAsync());
        }

        // GET: CereriMarire/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereMarire = await _context.CereriMarire
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereMarire == null)
            {
                return NotFound();
            }

            return View(cerereMarire);
        }

        // GET: CereriMarire/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CereriMarire/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Materie,Id,OwnerID,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereMarire cerereMarire)
        {
            if (ModelState.IsValid)
            {
                cerereMarire.Id = Guid.NewGuid();
                cerereMarire.OwnerID = _userManager.GetUserId(User);
                var isAuthorized = await _authorizationService.AuthorizeAsync(User,
                    cerereMarire, CereriOperatii.Create);
                if (!isAuthorized.Succeeded)
                {
                    return new ChallengeResult();
                }
                _context.Add(cerereMarire);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cerereMarire);
        }

        // GET: CereriMarire/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereMarire = await _context.CereriMarire.SingleOrDefaultAsync(m => m.Id == id);
            if (cerereMarire == null)
            {
                return NotFound();
            }
            return View(cerereMarire);
        }

        // POST: CereriMarire/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Materie,Id,Nume,Prenume,InitialaTatalui,Email,NumarMatricol,SerieBuletin,NumarBuletin,Stare")] CerereMarire cerereMarire)
        {
            cerereMarire.OwnerID = _userManager.GetUserId(User);
            if (id != cerereMarire.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cerereMarire);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CerereMarireExists(cerereMarire.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cerereMarire);
        }

        // GET: CereriMarire/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cerereMarire = await _context.CereriMarire
                .SingleOrDefaultAsync(m => m.Id == id);
            if (cerereMarire == null)
            {
                return NotFound();
            }

            return View(cerereMarire);
        }

        // POST: CereriMarire/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var cerereMarire = await _context.CereriMarire.SingleOrDefaultAsync(m => m.Id == id);
            _context.CereriMarire.Remove(cerereMarire);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CerereMarireExists(Guid id)
        {
            return _context.CereriMarire.Any(e => e.Id == id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetStatus(Guid id, Cerere.StareCerere status,String motiv)
        {
            var cerereMarire = await _context.CereriMarire.SingleOrDefaultAsync(m => m.Id == id);
            var isAuthorized = User.IsInRole("Admin") || User.IsInRole("Secretar");

            if (!isAuthorized)
            {
                return new ChallengeResult();
            }
            cerereMarire.Stare = status;
            _context.CereriMarire.Update(cerereMarire);
            await _context.SaveChangesAsync();

            if (status == Cerere.StareCerere.Aprobata)
            {
                await _emailSender.SendEmailAsync(cerereMarire.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereMarire.Id + " a fost " + status + ". \n\nDetaliile cererii:" +
                    "\n\tMaterie: " + cerereMarire.Materie +
                    "\n\tNume: " + cerereMarire.Nume +
                    "\n\tPrenume: " + cerereMarire.Prenume +
                    "\n\tInitiala tatalui: " + cerereMarire.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereMarire.NumarBuletin +
                    "\n\tNumar matricol: " + cerereMarire.NumarMatricol +
                    "\n\tSerie buletin: " + cerereMarire.SerieBuletin);
            }
            else
            {
                await _emailSender.SendEmailAsync(cerereMarire.Email, "Cerere " + status,
                    "Cererea cu id-ul: "
                    + cerereMarire.Id + " a fost " + status + ".\nMotivul respingerii: "+motiv+" \n\nDetaliile cererii:" +
                    "\n\tMaterie: " + cerereMarire.Materie +
                    "\n\tNume: " + cerereMarire.Nume +
                    "\n\tPrenume: " + cerereMarire.Prenume +
                    "\n\tInitiala tatalui: " + cerereMarire.InitialaTatalui +
                    "\n\tNumar buletin: " + cerereMarire.NumarBuletin +
                    "\n\tNumar matricol: " + cerereMarire.NumarMatricol +
                    "\n\tSerie buletin: " + cerereMarire.SerieBuletin);
            }
            return RedirectToAction("Index");
        }
    }
}
