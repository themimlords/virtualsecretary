﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Core.Domain
{
    public class CerereCTP : Cerere
    {
        [Required]
        public DateTime Data { get; set; }
    }
}
