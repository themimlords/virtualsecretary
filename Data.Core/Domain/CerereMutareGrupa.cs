﻿using System.ComponentModel.DataAnnotations;

namespace Data.Core.Domain
{
    public class CerereMutareGrupa : Cerere
    {
        [StringLength(3)]
        [RegularExpression(@"[1-3]{1}[AB][1-7]{1}")]
        [Required]
        public string GrupaVeche { get; set; }

        [StringLength(3)]
        [RegularExpression(@"[1-3]{1}[AB][1-7]{1}")]
        [Required]
        public string GrupaNoua { get; set; }
    }
}
