﻿using System.ComponentModel.DataAnnotations;

namespace Data.Core.Domain
{
    public class CerereAdeverintaStudent : Cerere
    {
        [Range(1, 3)]
        [Required]
        public int AnUniversitar { get; set; }

        [Required]
        public string MotivEliberare { get; set; }
    }
}
