﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Core.Domain
{
    public class Cerere
    {
        public Guid Id { get; set; }
        public string OwnerID { get; set; }

        [StringLength(25, MinimumLength = 3)]
        [RegularExpression(@"[a-zA-Z]+")]
        [Required]
        public string Nume { get; set; }

        [StringLength(25, MinimumLength = 3)]
        [RegularExpression(@"[a-zA-Z]+")]
        [Required]
        public string Prenume { get; set; }

        [StringLength(2)]
        [RegularExpression(@"[A-Z]+.")]
        [Required]
        public string InitialaTatalui { get; set; }

        public string Email { get; set; }

        [StringLength(16)]
        [RegularExpression(@"[0-9]{8}SL[0-9]{6}")]
        [Required]
        public string NumarMatricol { get; set; }

        [StringLength(2)]
        [RegularExpression(@"M[XZ]")]
        [Required]
        public string SerieBuletin { get; set; }

        [Range(100000,999999)]
        [Required]
        public int NumarBuletin { get; set; }
        public StareCerere Stare { get; set; }

        public Cerere()
        {
            Id = new Guid();
        }

        public enum StareCerere
        {
            Trimisa,
            Aprobata,
            Respinsa
        }
    }
}
