﻿using System.ComponentModel.DataAnnotations;

namespace Data.Core.Domain
{
    public class CerereSchimbareOptionale : Cerere
    {
        [Required]
        public string OptionalVechi { get; set; }

        [Required]
        public string OptionalNou { get; set; }
    }
}
