﻿using System.ComponentModel.DataAnnotations;

namespace Data.Core.Domain
{
    public class CerereMarire : Cerere
    {
        [Required]
        public string Materie { get; set; }
    }
}
